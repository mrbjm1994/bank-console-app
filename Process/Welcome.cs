using System;
using meyo_bank.Model;


namespace meyo_bank.Process
{
    public class Welcome
    {
        public static void splashscreen()
        {
            string fullname;
            int age;
            long phoneNumber;
            char sex;

            Console.WriteLine("Welcome to Meyo Bank");
            Console.WriteLine("Fill the form to set up your account");
            Console.Write("Enter your Full name - ");
            fullname = Console.ReadLine();
            Console.Write("How old are you? - ");
            age = int.Parse(Console.ReadLine());
            Console.Write("Enter your Phone number - ");
            phoneNumber = long.Parse(Console.ReadLine());
            Console.Write("What is your gender? - ");
            sex = char.Parse(Console.ReadLine());
            Console.WriteLine("Congratulations, Your account is successfully created");
            Customer customer = new Customer(fullname, age, phoneNumber, sex);
            Account account = new Account();
            Random random = new Random();
            account.Status = "Student Account";
            account.AccountBalance = 1000;
            account.AccountNumber = random.Next(009768656, 324367859);
            Console.WriteLine($"!!!!!Welcome {customer.fullName}, \n Your account number is {account.AccountNumber}!!!!!");
            Console.WriteLine($"We have given you NGN1000 signup bonus \n Your account balance is {account.AccountBalance}");
            Console.WriteLine($"Your account status is a {account.Status}");
            option.choice();

        }
    }
}