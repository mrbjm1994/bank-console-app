using System;
namespace meyo_bank.Model
{
    public class Account
    {
        public long AccountNumber{ get; set; }
        public long AccountBalance { get; set; }
        public string Status { get; set; }

        public DateTime created { get; set; }

        public static void deposit()
        {
            Account account = new Account();
            Console.Write("How much do you want to deposit - ");
            long amount = long.Parse(Console.ReadLine());
            Console.WriteLine($"You deposited {amount}");
            Console.WriteLine($"Your new balance is {amount + account.AccountBalance}");
        }
    }
    
    
}