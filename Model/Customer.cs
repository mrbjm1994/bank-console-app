using System;
namespace meyo_bank.Model
{
    public class Customer
    {
        public string fullName { get; set; }
        public int Age { get; set; }
        public long PhoneNumber { get; set; }
        public char Sex { get; set; }

        Account newUser = new Account();
        
        public void save(Account obj)
        {
            newUser = obj;
        }
        public Customer(string fullname, int age, long phoneNumber, char sex)
        {
            fullName = fullname;
            Age = age;
            PhoneNumber = phoneNumber;
            Sex = sex;

        }
    }
}